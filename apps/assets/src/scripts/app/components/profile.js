var csrftoken = Cookies.get('csrftoken');


PASSWORD_MISTMATCH = "password_mistmatch";
INVALID_PASSWORD = "invalid_password";

new Vue({
    el: "#profile",
    components: {

    },
    data: {
        user: USER,
        profileData: {},
        setPasswordData: {
            password: { value: null, error: null },
            confirm_password: { value: null, error: null }
        },
        passwordData: {
            oldpassword: { value: null, error: null },
            password1: { value: null, error: null },
            password2: { value: null, error: null }
        },
        emailData: {
            new_email: { value: null, error: null },
            password: { value: null, error: null }
        }
    },
    methods: {



        makeProfileData: function () {
            var data = new FormData();

            if (!!this.user.username)
                data.append("username", this.user.username);

            if (!!this.user.first_name)
                data.append("first_name", this.user.first_name);

            if (!!this.user.last_name)
                data.append("last_name", this.user.last_name);

            if (!!this.$refs.photoFile && this.$refs.photoFile.files.length > 0)
                data.append('photo', this.$refs.photoFile.files[0]);

            return data;
        },

        isValidImage: function (file) {
            if(!file.type.match('image.*')) {
                toastr.error("Escoja una imagen", "No valido", this.notification);
                return false;
            }else if(file.size > 3145728){
                toastr.error("La imagen no puede ser mayor a 3MB", "No valido", this.notification)
            }else{
                return true;
            }
        },

        checkFile: function (e) {
            if (e.target.files.length > 0){
                var file = e.target.files[0];
                if (this.isValidImage(file)) this.showPreview(file);
            }
        },

        showPreview: function(file) {
            var fileReader = new FileReader();
            var self = this;
            fileReader.onload = function (e) {
                $(self.$refs.previewImage).attr('src', e.target.result);
            };
            fileReader.readAsDataURL(file);
        },

        endpoint: function (endpoint) {
            if (!!endpoint)
                return BASE_API_URL + "me/" + endpoint + "/";
            return BASE_API_URL + "me/";
        },

        updateProfile: function () {
            var data = this.makeProfileData();

            this.$http.put(this.endpoint("profile"), data).then(function(response){
                this.user = response.body;

                toastr.info("Perfil actualizado", "Actualizado", this.notification)
            }, function(responseError){
                toastr.error(responseError.body.message, "Error al actualizar", this.notification);
            });
        },

        setPassword: function () {
            this.$http.post(this.endpoint("password"), {
                password: this.setPasswordData.password.value,
                confirm_password: this.setPasswordData.confirm_password.value
            }).then(function(response){
                this.clearSetPasswordData(true);
                this.user = response.body;
                toastr.info("Contraseña configurada", "Actualizado", this.notification);
                location.reload(true);
            }, function(responseError){
                this.clearSetPasswordData(false);

                if(!!responseError.body.password) this.setPasswordData.password.error = _.first(responseError.body.password).message;
                if(!!responseError.body.confirm_password) this.setPasswordData.confirm_password.error = _.first(responseError.body.confirm_password).message;

                 if(!!responseError.body.code) {
                    switch (responseError.body.code){
                        case PASSWORD_MISTMATCH:
                            this.setPasswordData.confirm_password.error = responseError.body.message;
                            break;
                    }
                }

            });
        },

        updatePassword: function () {
            this.$http.put(this.endpoint("password"), {
                password: this.passwordData.oldpassword.value,
                new_password: this.passwordData.password1.value,
                confirm_password: this.passwordData.password2.value
            }).then(function(response){
                this.clearPasswordData(true);
                this.user = response.body;
                toastr.info("Contraseña actualizada", "Actualizado", this.notification)
            }, function(responseError){
                this.clearPasswordData(false);

                if(!!responseError.body.password) this.passwordData.oldpassword.error = _.first(responseError.body.password).message;
                if(!!responseError.body.new_password) this.passwordData.password1.error = _.first(responseError.body.new_password).message;
                if(!!responseError.body.confirm_password) this.passwordData.password2.error = _.first(responseError.body.confirm_password).message;

                if(!!responseError.body.code) {
                    switch (responseError.body.code){
                        case INVALID_PASSWORD:
                            this.passwordData.oldpassword.error = responseError.body.message;
                            break;
                        case PASSWORD_MISTMATCH:
                            this.passwordData.password2.error = responseError.body.message;
                            break;
                    }
                }

            });
        },
        updateEmail: function () {
            this.$http.put(this.endpoint("email"), {
                password: this.emailData.password.value,
                new_email: this.emailData.new_email.value
            }).then(function(response){
                this.clearEmailData(true);
                this.user = response.body;
                toastr.info("Correo Electrónico actualizado", "Actualizado", this.notification)
            }, function(responseError){
                this.clearEmailData(false);

                if(!!responseError.body.password) this.emailData.password.error = _.first(responseError.body.password).message;
                if(!!responseError.body.new_email) this.emailData.new_email.error = _.first(responseError.body.new_email).message;

                if(!!responseError.body.code) {
                    switch (responseError.body.code){
                        case INVALID_PASSWORD:
                            this.emailData.password.error = responseError.body.message;
                            break;
                    }
                }
            });
        },

        clearSetPasswordData: function (clearValue) {
            this.setPasswordData.password.error = null;
            this.setPasswordData.confirm_password.error = null;

            if(clearValue) {
                this.setPasswordData.password.value = null;
                this.setPasswordData.confirm_password.value = null;
            }
        },

        clearPasswordData: function (clearValue) {
            this.passwordData.oldpassword.error = null;
            this.passwordData.password1.error = null;
            this.passwordData.password2.error = null;

            if(clearValue) {
                this.passwordData.oldpassword.value = null;
                this.passwordData.password1.value = null;
                this.passwordData.password2.value = null;
            }
        },

        clearEmailData: function (clearValue) {
            this.emailData.password.error = null;
            this.emailData.new_email.error = null;

            if(clearValue) {
                this.emailData.password.value = null;
                this.emailData.new_email.value = null;
            }
        }
    },
    mounted: function () {

    }
});