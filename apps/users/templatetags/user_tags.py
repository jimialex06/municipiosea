# -*- encoding:utf-8 -*-

from django import template
from django.utils.safestring import mark_safe

from apps.users.api.v1.serializers import UserSerializer
import json

register = template.Library()


@register.simple_tag(takes_context=True)
def user(context, user=None):
    if user:
        to_json = user
    else:
        if "user_in" in context:
            to_json = context["user_in"]
        else:
            to_json = context["user"]
    return mark_safe(UserSerializer(to_json).data)


@register.filter(name='json')
def json_dumps(data):
    import ipdb; ipdb.set_trace()
    return mark_safe(json.dumps(data))
