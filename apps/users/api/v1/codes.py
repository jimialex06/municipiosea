# -*- encoding:utf-8 -*-

from django.utils.translation import ugettext_lazy as _
from rest_framework import status


# ~ SUCCESS
# --------------------------------------------

USERNAME_UNAVAILABLE = {
    "code": "username_unavailable",
    "detail": _("Este nombre de usuario está siendo utilizado por otro usuario"),
}

EMAIL_UNAVAILABLE = {
    "code": "email_unavailable",
    "detail": _("Este correo está siendo utilizado por otro usuario"),
}

INVALID_PASSWORD = {
    "code": "invalid_password",
    "detail": _("Por favor, escribe tu contraseña actual.")
}

PASSWORD_MISTMATCH = {
    "code": "password_mistmatch",
    "detail": _("Las contraseñas no coinciden")
}


# ~ SUCCESS
# --------------------------------------------
PASSWORD_UPDATED = {
    "code": "password_updated",
    "detail": _("Contraseña actualizada con éxito"),
    "status": status.HTTP_201_CREATED
}

PASSWORD_ADDED = {
    "code": "password_added",
    "detail": _("Contraseña configurada con éxito"),
    "status": status.HTTP_201_CREATED
}

EMAIL_UPDATED = {
    "code": "email_updated",
    "detail": _("Correo actualizado  con éxito"),
    "status": status.HTTP_201_CREATED
}

ACCOUNT_CANCELLED = {
    "code": "account_cancelled",
    "detail": _("Su cuenta fue cancelada."),
    "status": status.HTTP_201_CREATED
}

SESSION_CLOSED = {
    "code": "session_closed",
    "detail": _("Sesión cerrada con éxito"),
    "status": status.HTTP_201_CREATED
}
