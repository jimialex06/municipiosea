# -*- encoding:utf-8 -*-

from django.urls import path

from .views import AccountViewSet

app_name = 'users'
urlpatterns = [

    path('/me/profile/', AccountViewSet.as_view({
        "get": "get_profile", "put": "update_profile"}), name="profile"),


    path('/me/email/', AccountViewSet.as_view({"put": "update_email"}), name="email"),
    path('/me/password/', AccountViewSet.as_view({
        "put": "update_password", "post": "set_password"}), name="password"),


    # path('me/cancel-account/', AccountViewSet.as_view({"get": "cancel", "post": "cancel_confirm"}), name="cancel"),
    # path('users/search/', UserList.as_view(), name="search"),


]