# -*- coding: utf-8 -*-
import yaml
from rest_framework.compat import coreapi, urlparse
from rest_framework.schemas import SchemaGenerator
from rest_framework.decorators import api_view, renderer_classes
from rest_framework import response, renderers
from openapi_codec import OpenAPICodec


class CustomSchemaGenerator(SchemaGenerator):
    def get_link(self, path, method, view):
        """
        __doc__ in yaml format, eg:
        desc: the desc of this api.
        parameters:
        - name: mobile
          desc: the mobile number
          type: string
          required: true
          location: query
        - name: promotion
          desc: the activity id
          type: int
          required: true
          location: form
        """
        fields = self.get_path_fields(path, method, view)
        yaml_doc = None
        func = getattr(view, view.action) if getattr(view, 'action', None) else None

        if func and func.__doc__:
            try:
                yaml_doc = yaml.load(func.__doc__)
            except Exception as e:
                yaml_doc = None

        if yaml_doc and 'desc' in yaml_doc:
            desc = yaml_doc.get('desc', '')
            _method_desc = desc
            params = yaml_doc.get('parameters', [])
            for i in params:
                _name = i.get('name')
                _desc = i.get('desc')
                _required = i.get('required', True)
                _type = i.get('type', 'string')
                _location = i.get('location', 'query')

                f = coreapi.Field(
                    name=_name,
                    location=_location,
                    required=_required,
                    description=_desc,
                    type=_type
                )
                fields.append(f)
        else:
            _method_desc = func.__doc__ if func and func.__doc__ else ''
            fields += self.get_serializer_fields(path, method, view)

        if yaml_doc and 'encoding' in yaml_doc:
            enc = yaml_doc.get('encoding', None)

        else:
            enc = None

        fields += self.get_pagination_fields(path, method, view)
        fields += self.get_filter_fields(path, method, view)

        if enc:
            encoding = enc
        else:
            if fields and any([field.location in ('form', 'body') for field in fields]):
                encoding = self.get_encoding(path, method, view)
            else:
                encoding = None

        if self.url and path.startswith('/'):
            path = path[1:]

        return coreapi.Link(
            url=urlparse.urljoin(self.url, path),
            action=method.lower(),
            encoding=encoding,
            fields=fields,
            description=_method_desc
        )


class SwaggerRenderer(renderers.BaseRenderer):
    media_type = 'application/openapi+json'
    format = 'swagger'

    def render(self, data, media_type=None, renderer_context=None):
        codec = OpenAPICodec()
        return codec.dump(data)

# generator = schemas.SchemaGenerator(title='API')
generator = CustomSchemaGenerator(title='Dev API',
                                  description="<b>Ara360</b> Developer <a href=\"https://xiberty.com\">API</a>")


@api_view()
@renderer_classes([SwaggerRenderer])
def schema_view(request):
    schema = generator.get_schema(request)
    return response.Response(schema, content_type="application/json")

