# -*- encoding:utf-8 -*-

from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet, ModelViewSet, ViewSet


class PermissionlMixin(object):
    """
    Mixed permission base model allowing for action level
    permission control. Subclasses may define their permissions
    by creating a 'permission_classes_by_action' variable.

    Example:
    permissions_by_action = {'list': [AllowAny],
                                   'create': [IsAdminUser]}
    """
    permissions_by_action = {}

    def get_permissions(self):
        try:
            return [permission() for permission in self.permissions_by_action[self.action]]
        except KeyError:
            return [permission() for permission in self.permission_classes]


class ModelDetailListViewSet(
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    GenericViewSet):
    pass


class ModelListViewSet(
    mixins.ListModelMixin,
    GenericViewSet):
    pass


class ModelUpdateListViewSet(
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    GenericViewSet):
    pass


class ModelRetrieveListViewSet(
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    GenericViewSet):
    pass


class ModelRetrieveUpdateListViewSet(
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.ListModelMixin,
    GenericViewSet):
    pass


class ModelCreateRetrieveListViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.ListModelMixin,
    GenericViewSet):
    pass


class ModelCreateListViewSet(
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    GenericViewSet):
    pass


class PermissionModelViewSet(PermissionlMixin, ModelViewSet):
    pass


class PermissionViewSet(PermissionlMixin, GenericViewSet):
    pass

