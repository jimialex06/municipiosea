# -*- encoding:utf-8 -*-
from allauth.utils import get_username_max_length
from django.contrib.auth import get_user_model, authenticate
from django.conf import settings
from django.contrib.auth.forms import SetPasswordForm
from django.contrib.auth.tokens import default_token_generator
from django.db.models import Q
from django.utils.http import urlsafe_base64_decode as uid_decoder
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import force_text
from rest_framework.authtoken.models import Token

from rest_framework import serializers

from apps.auths.messages import AuthMessages
from apps.contrib.utils.strings import get_uuid, get_lapse
from apps.auths.models import UserAction
from apps.auths import codes
from apps.contrib.api.exceptions import ValidationError, NotFound


try:
    from allauth.account import app_settings as allauth_settings
    from allauth.utils import (
        email_address_exists, get_username_max_length)
    from allauth.account.adapter import get_adapter
    from allauth.account.utils import setup_user_email
except ImportError:
    raise ImportError("allauth needs to be added to INSTALLED_APPS.")


UserModel = get_user_model()


class LoginSerializer(serializers.Serializer):
    login = serializers.CharField()
    password = serializers.CharField(style={'input_type': 'password'})

    def validate(self, attrs):
        login = attrs.get('login')
        password = attrs.get('password')

        user = None
        print("********************validate**********************")
        if UserModel.objects.filter(Q(username=login) | Q(email=login)).exists():
            user = UserModel.objects.get(Q(username=login) | Q(email=login))
            if not user.is_active:
                raise ValidationError(**codes.INACTIVE_ACCOUNT)

            if "allauth" in settings.INSTALLED_APPS:
                from allauth.account import app_settings
                if hasattr(settings, "EMAIL_VERIFICATION") and \
                   settings.EMAIL_VERIFICATION == app_settings.EmailVerificationMethod.MANDATORY:

                    email_address = user.emailaddress_set.get(email=user.email)
                    if not email_address.verified:
                        raise ValidationError(**codes.UNVERIFIED_EMAIL)

            if not user.check_password(password):
                raise ValidationError(**codes.INVALID_CREDENTIALS)

            attrs['user'] = user
            return attrs
        else:
            raise ValidationError(**codes.INVALID_CREDENTIALS)


class PasswordResetSerializer(serializers.Serializer):
    """
    Serializer for requesting a password reset e-mail.
    """
    login = serializers.CharField()
    redirect_uri = serializers.URLField(required=False)

    def validate(self, attrs):
        if not UserModel.objects.filter(Q(username=attrs["login"]) | Q(email=attrs["login"])).exists():
            raise NotFound(**codes.USER_NOT_FOUND)

        user = UserModel.objects.get(Q(username=attrs["login"]) | Q(email=attrs["login"]))

        if UserAction.objects.filter(user=user, type=UserAction.ACTION_RESET_PASSWORD).exists():
            action = UserAction.objects.get(user=user, type=UserAction.ACTION_RESET_PASSWORD)
        else:
            action = UserAction(user=user, type=UserAction.ACTION_RESET_PASSWORD)

        action.token = get_uuid()
        action.creation_date, action.expiration_date = get_lapse()
        action.save()


        attrs["action"] = action
        return attrs


class PasswordResetConfirmSerializer(serializers.Serializer):
    """
    Serializer for requesting a password reset e-mail.
    """
    token = serializers.CharField()
    password1 = serializers.CharField(max_length=128)
    password2 = serializers.CharField(max_length=128)

    def validate(self, attrs):

        if not UserAction.objects.filter(token=attrs["token"],
                                         type=UserAction.ACTION_RESET_PASSWORD).exists():
            raise ValidationError(**codes.INVALID_TOKEN)

        if attrs["password1"] != attrs["password2"]:
            raise ValidationError(**codes.PASSWORD_MISTMATCH)

        action = UserAction.objects.get(token=attrs["token"], type=UserAction.ACTION_RESET_PASSWORD)
        attrs["password"] = attrs["password2"]
        attrs["action"] = action
        return attrs


class RegisterSerializer(serializers.Serializer):
    username = serializers.CharField(
        max_length=get_username_max_length(),
        min_length=allauth_settings.USERNAME_MIN_LENGTH,
        required=allauth_settings.USERNAME_REQUIRED
    )
    email = serializers.EmailField(required=allauth_settings.EMAIL_REQUIRED)
    password1 = serializers.CharField(write_only=True)
    password2 = serializers.CharField(write_only=True)

    def validate_username(self, username):
        username = get_adapter().clean_username(username)
        return username

    def validate_email(self, email):
        email = get_adapter().clean_email(email)
        if allauth_settings.UNIQUE_EMAIL:
            if email and email_address_exists(email):
                raise ValidationError(**codes.UNAVAILABLE_EMAIL)
        return email

    def validate_password1(self, password):
        return get_adapter().clean_password(password)

    def validate(self, data):
        if data['password1'] != data['password2']:
            raise ValidationError(**codes.PASSWORD_MISTMATCH)
        return data

    def get_cleaned_data(self):
        return {
            'username': self.validated_data.get('username', ''),
            'password1': self.validated_data.get('password1', ''),
            'email': self.validated_data.get('email', '')
        }

    def save(self, request):
        adapter = get_adapter()
        user = adapter.new_user(request)
        self.cleaned_data = self.get_cleaned_data()
        adapter.save_user(request, user, self)
        setup_user_email(request, user, [])
        return user


class VerifyEmailSerializer(serializers.Serializer):
    key = serializers.CharField()


class TokenSerializer(serializers.Serializer):
    token = serializers.CharField()

    def validate(self, attrs):

        if not Token.objects.filter(key=attrs['token']).exists():
            raise ValidationError(**codes.INVALID_TOKEN)
        else:
            attrs["token"] = Token.objects.get(key=attrs['token'])

        return attrs
