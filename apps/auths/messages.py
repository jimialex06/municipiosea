# -*- encoding:utf-8 -*-

from django.template.loader import render_to_string
from django.urls import reverse

from apps.contrib.email import send_email
from apps.contrib.utils.strings import get_hostname


class AuthMessages(object):

    @staticmethod
    def account_activation(request, action):
        path = "%s%s" % get_hostname(request), reverse("account-activation", kwargs={"token": action.token})
        context = {"request": request, "action": action, "path": path}
        subject = render_to_string(
            template_name="emails/account/activation/subject.txt",
            context=None
        )
        text_body = render_to_string(
            template_name="emails/account/activation/message.txt",
            context=context
        )
        html_body = render_to_string(
            template_name="emails/account/activation/message.html",
            context=context
        )
        send_email(subject, [action.user.email], text_body, html_body)

    @staticmethod
    def send_welcome(request, action):
        context = {"request": request, "action": action}
        subject = render_to_string(
            template_name="emails/account/welcome/subject.txt",
            context=None
        )
        text_body = render_to_string(
            template_name="emails/account/welcome/message.txt",
            context=context
        )
        html_body = render_to_string(
            template_name="emails/account/welcome/message.html",
            context=context
        )
        send_email(subject, [action.user.email], text_body, html_body)

    @staticmethod  # VALID
    def reset_password(request, action, redirect_uri):
        """
        :param request: objecto de la petición
        :param action: Es la instancia del modelo que contendra el token y el tipo de accion
        :param redirect_uri: Es una url personzalizada donde recibir el token para Aplicaiones SPA
        """
        path = "%s/%s" % (redirect_uri, action.token)
        context = {"request": request, "action": action, "password_reset_url": path}

        subject = render_to_string(
            template_name="emails/account/password_reset/subject.txt",
            context=context
        )
        text_body = render_to_string(
            template_name="emails/account/password_reset/message.txt",
            context=context
        )
        html_body = render_to_string(
            template_name="emails/account/password_reset/message.html",
            context=context
        )
        send_email(subject, [action.user.email], text_body, html_body)

    @staticmethod
    def cancel_account(request, action):
        path = reverse("cancel-account", kwargs={"token": action.token})
        context = {"path": path}

        subject = render_to_string(
            template_name="emails/account/cancel/subject.txt",
            context=context
        )
        text_body = render_to_string(
            template_name="emails/account/cancel/message.txt",
            context=context
        )
        html_body = render_to_string(
            template_name="emails/account/cancel/message.html",
            context=context
        )
        send_email(subject, [action.user.email], text_body, html_body)

