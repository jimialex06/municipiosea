# -*- encoding:utf-8 -*-

"""
Production Configurations

- Use Amazon's S3 for storing static files and uploaded media
- Use mailgun to send emails
- Use Redis for cache

- Use sentry for error logging

"""
from __future__ import absolute_import, unicode_literals

import logging

from os.path import join

from .common import *

DEBUG = False
env = environ.Env()
env_file = join(PROJECT_PATH, ".environment")
env.read_env(env_file)

if not DEBUG:
    print("deploy")
    PROJECT_PATH = dirname(dirname(dirname(__file__)))
    #PROJECT_PATH = '/home/municipiosae/'
    APPS_PATH = join(PROJECT_PATH, "apps")
    print ('*****project path********',PROJECT_PATH)
    print ('*****apps path********',APPS_PATH)


#  GENERAL CONFIGURATION
# ------------------------------------------------------------------------------

SECRET_KEY = "yj6o@&vrar6^l1u68!bxumj@xr4&5)zzk!h1qw&63^&@2d3w@#"
#SECRET_KEY = env('DJANGO_SECRET_KEY')
#ALLOWED_HOSTS = env.list('104.248.214.206', 'DJANGO_ALLOWED_HOSTS', default=[PROJECT_DOMAIN, '104.248.214.206', "localhost", "127.0.0.1"])
# ALLOWED_HOSTS = ["sereci.oep.org.bo", "localhost", "127.0.0.1"]
ALLOWED_HOSTS = ["104.248.214.206", "localhost", "127.0.0.1"]

"""
#  EMAIL CONFIGURATION
# ------------------------------------------------------------------------------
EMAIL_SUBJECT_PREFIX = env('EMAIL_SUBJECT_PREFIX', default='[Xiberty] ')
SERVER_EMAIL = env('SERVER_EMAIL', default=DEFAULT_FROM_EMAIL)
INSTALLED_APPS += ["anymail"]
ANYMAIL = {
    "MAILGUN_API_KEY": env('MAILGUN_API_KEY'),
    "MAILGUN_SEND_DEFAULTS": {
        "esp_extra": {"sender_domain": "mg.xiberty.com"}
    }
}
"""
DEFAULT_FROM_EMAIL = env('DEFAULT_FROM_EMAIL', default='Support <support@xiberty.com>')
EMAIL_BACKEND = "anymail.backends.mailgun.MailgunBackend"  # or sendgrid.SendGridBackend, or...

"""
#  LOGGING CONFIGURATION
# ------------------------------------------------------------------------------
INSTALLED_APPS += (
    'raven.contrib.django.raven_compat',
)
SENTRY_DSN = env('DJANGO_SENTRY_DSN')
SENTRY_CLIENT = env('SENTRY_CLIENT', default='raven.contrib.django.raven_compat.DjangoClient')
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'root': {
        'level': 'WARNING',
        'handlers': ['sentry'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s  %(asctime)s  %(module)s '
                      '%(process)d  %(thread)d  %(message)s'
        },
    },
    'handlers': {
        'sentry': {
            'level': 'ERROR', # To capture more than ERROR, change to WARNING, INFO, etc.
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
            'tags': {'custom-tag': 'x'},
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'django.db.backends': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
        },
        'raven': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'sentry.errors': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
    },
}
SENTRY_CELERY_LOGLEVEL = env.int('DJANGO_SENTRY_LOG_LEVEL', logging.INFO)
RAVEN_CONFIG = {
    'CELERY_LOGLEVEL': env.int('DJANGO_SENTRY_LOG_LEVEL', logging.INFO),
    'DSN': SENTRY_DSN
}
#  MIDDLEWARES
# ------------------------------------------------------------------------------
# See: https://whitenoise.readthedocs.io/
# WHITENOISE_MIDDLEWARE = ['whitenoise.middleware.WhiteNoiseMiddleware']
# MIDDLEWARE = WHITENOISE_MIDDLEWARE + MIDDLEWARE


RAVEN_MIDDLEWARE = ['raven.contrib.django.raven_compat.middleware.SentryResponseErrorIdMiddleware',
    'raven.contrib.django.raven_compat.middleware.Sentry404CatchMiddleware',]
MIDDLEWARE = RAVEN_MIDDLEWARE + MIDDLEWARE
"""


# SECURITY CONFIGURATION
# ------------------------------------------------------------------------------
# See https://docs.djangoproject.com/en/dev/ref/middleware/#module-django.middleware.security
# and https://docs.djangoproject.com/en/dev/howto/deployment/checklist/#run-manage-py-check-deploy

# ------------------------------------------------------------------------------
# THIS CONFIGURATION ONLY REQUIRED IN A ALONE SERVER INSTALLATION IS NOT REQUIRED
# WHEN YOUR INSTALLATION IS BEHIND LOAD BALANCER
# ------------------------------------------------------------------------------
# set this to 60 seconds and then to 518400 when you can prove it works
# SECURE_HSTS_SECONDS = 60
# SECURE_HSTS_INCLUDE_SUBDOMAINS = env.bool('DJANGO_SECURE_HSTS_INCLUDE_SUBDOMAINS', default=True)
# SECURE_CONTENT_TYPE_NOSNIFF = env.bool('DJANGO_SECURE_CONTENT_TYPE_NOSNIFF', default=True)
# SECURE_BROWSER_XSS_FILTER = True
# SESSION_COOKIE_SECURE = True
# SESSION_COOKIE_HTTPONLY = True
# SECURE_SSL_REDIRECT = env.bool('DJANGO_SECURE_SSL_REDIRECT', default=False)
# CSRF_COOKIE_SECURE = True
# CSRF_COOKIE_HTTPONLY = True
# X_FRAME_OPTIONS = 'DENY'


# STORAGE CONFIGURATION
# ------------------------------------------------------------------------------
# Uploaded Media Files
# ------------------------
# See: http://django-storages.readthedocs.io/en/latest/index.html
# INSTALLED_APPS += ['storages', ]

# AWS_ACCESS_KEY_ID = env('DJANGO_AWS_ACCESS_KEY_ID')
# AWS_SECRET_ACCESS_KEY = env('DJANGO_AWS_SECRET_ACCESS_KEY')
# AWS_STORAGE_BUCKET_NAME = env('DJANGO_AWS_STORAGE_BUCKET_NAME')
# AWS_S3_HOST = env('DJANGO_AWS_S3_HOST', default="s3.amazonaws.com")
# AWS_AUTO_CREATE_BUCKET = True
# AWS_QUERYSTRING_AUTH = False

# AWS cache settings, don't change unless you know what you're doing:
# AWS_EXPIRY = 60 * 60 * 24 * 7

# TODO See: https://github.com/jschneier/django-storages/issues/47
# Revert the following and use str after the above-mentioned bug is fixed in
# either django-storage-redux or boto
# control = 'max-age=%d, s-maxage=%d, must-revalidate' % (AWS_EXPIRY, AWS_EXPIRY)
# AWS_HEADERS = {
#     'Cache-Control': bytes(control, encoding='latin-1')
# }

# URL that handles the media served from MEDIA_ROOT, used for managing
# stored files.
# MEDIA_URL = 'https://s3.amazonaws.com/%s/' % AWS_STORAGE_BUCKET_NAME
# DEFAULT_FILE_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'


# STATIC ASSETS
# ------------------------------------------------------------------------------
#STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'
#STATICFILES_STORAGE = STATIC_URL
"""
# OPBEAT
# ------------------------------------------------------------------------------
# See https://opbeat.com/languages/django/
INSTALLED_APPS += ['opbeat.contrib.django', ]
OPBEAT = {
    'ORGANIZATION_ID': env('DJANGO_OPBEAT_ORGANIZATION_ID'),
    'APP_ID': env('DJANGO_OPBEAT_APP_ID'),
    'SECRET_TOKEN': env('DJANGO_OPBEAT_SECRET_TOKEN')
}
MIDDLEWARE = ['opbeat.contrib.django.middleware.OpbeatAPMMiddleware', ] + MIDDLEWARE
"""

# COMPRESSOR
# ------------------------------------------------------------------------------
#COMPRESS_STORAGE = 'storages.backends.s3boto3.S3Boto3Storage'
#COMPRESS_URL = STATIC_URL
#COMPRESS_ENABLED = env.bool('COMPRESS_ENABLED', default=True)


# TEMPLATE CONFIGURATION
# ------------------------------------------------------------------------------
# See:
# https://docs.djangoproject.com/en/dev/ref/templates/api/#django.template.loaders.cached.Loader
TEMPLATES[0]['OPTIONS']['loaders'] = [
    ('django.template.loaders.cached.Loader', [
        'django.template.loaders.filesystem.Loader', 'django.template.loaders.app_directories.Loader', ]),
]


# CACHING
# ------------------------------------------------------------------------------
CACHES = {
    'default': {
        'BACKEND': 'django_redis.cache.RedisCache',
        'LOCATION': env('REDIS_CACHE_URL', default='redis://redis:6379/1'),
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
            'IGNORE_EXCEPTIONS': True,  # mimics memcache behavior.
                                        # http://niwinz.github.io/django-redis/latest/#_memcached_exceptions_behavior
        }
    }
}


# ADMIN
# ------------------------------------------------------------------------------
ADMIN_URL = env('DJANGO_ADMIN_URL', default='admin/')

"""
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': env('POSTGRES_DATABASE', default='postgres'),
        'USER': env('POSTGRES_USER', default='postgres'),
        'PASSWORD': env('POSTGRES_PASSWORD', default='11002299338844775566'),
        'HOST': env('DJANGO_DATABASE_HOST', default='postgres'),
    }
}
"""
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'municipiosea_app',
        'USER': 'municipiosea_user',
        'PASSWORD': 'postgres',
        'HOST': 'localhost',
        'PORT': '',

    }
}

DATABASES['default']['ATOMIC_REQUESTS'] = True


# TASK MANAGEMENT CONFIGURATION
# ------------------------------------------------------------------------------
CELERY_BROKER_URL = env('CELERY_BROKER_URL', default='redis://redis:6379/1')
CELERY_RESULT_BACKEND = env('CELERY_BROKER_URL', default='redis://redis:6379/1')
CONSTANCE_REDIS_CONNECTION = env('REDIS_CACHE_URL', default='redis://redis:6379/0')

PROJECT_DOMAIN = env("PROJECT_DOMAIN", default="http://municipiosea.com")

# STATIC FILE CONFIGURATION
# ------------------------------------------------------------------------------
"""
if not DEBUG:
    STATIC_ROOT = '/home/municipiosea/apps/'
    STATIC_URL = '/assets/'
    print(STATIC_ROOT,'***************')
    print(STATIC_URL,'***************')

    STATICFILES_DIRS = (
        join(APPS_PATH, 'assets'),
        #"/home/municipiosea/apps/assets",
    )
    STATICFILES_FINDERS = (
        'django.contrib.staticfiles.finders.FileSystemFinder',
        'django.contrib.staticfiles.finders.AppDirectoriesFinder',

        # Compressor
        'compressor.finders.CompressorFinder',
    )
"""